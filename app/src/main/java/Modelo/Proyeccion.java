package Modelo;

import android.database.Cursor;

import com.example.listview92.Alumno;
import com.example.listview92.AlumnoItem;

import java.util.ArrayList;

public interface Proyeccion {

    public Alumno getAlumno(String matricula);
    public ArrayList<Alumno> allAlumnos();
    public Alumno readAlumno(Cursor cursor);

}
